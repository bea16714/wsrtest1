function toggleMenu() {
    const menuToggle = document.querySelector('.toggle');
    const sidebar = document.querySelector('.sidebar');
    const header = document.querySelector('.header');

    menuToggle.classList.toggle('active')
    sidebar.classList.toggle('active')
    header.classList.toggle('active')
    header.style.display = 'none'

}

(function() {

    window.addEventListener('scroll', function() {
        var elem = document.querySelector('.header')
        var logo = document.querySelector('.header .logo')
        var mobile = document.querySelector('.header .mobile')
        var email = document.querySelector('.header .email')
        var sci = document.querySelectorAll('.soc')
        var header_schedule = document.querySelector('.header_schedule')
        body = document.body;
        html = document.documentElement;

        if (window.screen.width > 991 && document.querySelector('.header.active') == null)
        {
            var clientHeight = document.getElementById('home').clientHeight;
            var clientHeight2 = document.getElementById('about').clientHeight;
            var clientHeight3 = document.getElementById('price').clientHeight;
            var clientHeight4 = document.getElementById('work').clientHeight;

            var scrollTop = html.scrollTop || 0;
            if (scrollTop > (clientHeight - 1)) { // 2 блок
                elem.style.display = 'block';
                elem.style.background = '#000';
                logo.style.background = '#fff';
                logo.style.color = '#000';
                mobile.style.color = '#fff';
                email.style.color = '#fff';
                header_schedule.style.color = '#fff';
                sci.forEach(function (item, i, sci) {
                    item.style.filter = 'invert(1)';
                });

                var scrollTop2 = html.scrollTop - clientHeight;  // 3 и 4 блок
                if (scrollTop2 > (clientHeight2 - 1)) {
                    elem.style.background = '#fff';
                    logo.style.background = '#000';
                    logo.style.color = '#fff';
                    mobile.style.color = '#000';
                    email.style.color = '#000';
                    header_schedule.style.color = '#000';
                    sci.forEach(function (item, i, sci) {
                        item.style.filter = 'invert(0)';
                    });

                    var scrollTop3 = html.scrollTop - clientHeight - clientHeight2 - clientHeight3 - clientHeight4;  // 5 блок
                    if (scrollTop3 > (-1)) {

                        elem.style.background = '#000';
                        logo.style.background = '#fff';
                        logo.style.color = '#000';
                        mobile.style.color = '#fff';
                        email.style.color = '#fff';
                        header_schedule.style.color = '#fff';
                        sci.forEach(function (item, i, sci) {
                            item.style.filter = 'invert(1)';
                        });
                    }

                }
            } else { // 1 блок
                elem.style.display = 'none';
            }
        }
        else {
            elem.style.display = 'none';
        }
    });
})();